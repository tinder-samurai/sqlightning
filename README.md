# **SQLightning** [![codebeat badge](https://codebeat.co/badges/ce4e6e00-cb0b-4b99-bb15-b56a01305d1c)](https://codebeat.co/projects/github-com-henryco-sqlightning-master) [![GitHub license](https://img.shields.io/badge/license-MIT-red.svg)](https://raw.githubusercontent.com/henryco/SQLightning/master/LICENSE) [![Maven Central](https://img.shields.io/maven-central/v/com.github.henryco/sqlightning.svg)](http://repo1.maven.org/maven2/com/github/henryco/sqlightning/)
###### Reflective android java library that allows you to work with the built-in database easier way.

____

## Installation 
First you need to add maven central repository into your project and then add dependency:  
  
  
  

**Gradle**
```Groovy
    compile 'com.github.henryco:sqlightning:1.1.3'
```

**Maven**

```XML
    <dependency>
        <groupId>com.github.henryco</groupId>
        <artifactId>sqlightning</artifactId>
        <version>1.1.3</version>
    </dependency>
```

____

## Getting started

* [**How into in 5 min**](https://bitbucket.org/tinder-samurai/sqlightning/wiki/How%20into%20in%205%20min)
* [**Advanced features**](https://bitbucket.org/tinder-samurai/sqlightning/wiki/Advanced%20features)
* [**List of Annotations**](https://bitbucket.org/tinder-samurai/sqlightning/wiki/List%20of%20annotations)

____

![Screenshot](https://bitbucket.org/tinder-samurai/sqlightning/raw/a9e179ce3f0809730637e01eacb0fa65e3b5c5bb/howto/Screenshot%20from%202017-05-23%2023-59-23.png)
![Screenshot](https://bitbucket.org/tinder-samurai/sqlightning/raw/a9e179ce3f0809730637e01eacb0fa65e3b5c5bb/howto/Screenshot%20from%202017-05-24%2000-29-26.png)